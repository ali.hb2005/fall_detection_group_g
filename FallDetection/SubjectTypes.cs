﻿using System.ComponentModel;

namespace FallDetection
{
    public enum SubjectTypes
    {
        [Description("Subject type is unknown")]
        None,
        [Description("Adult between 19 and 30 years old")] SA,
        [Description("Elderly between 60 and 75 years old")] SE
    }
}