﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using Microsoft.Win32;

namespace FallDetection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            _redOn = new BitmapImage(new Uri("pack://application:,,,/FallDetection;component/Assets/LED-RedOn.png"));
            _redOff = new BitmapImage(new Uri("pack://application:,,,/FallDetection;component/Assets/LED-RedOff.png"));
            _greenOn = new BitmapImage(new Uri("pack://application:,,,/FallDetection;component/Assets/LED-GreenOn.png"));
            _greenOff = new BitmapImage(new Uri("pack://application:,,,/FallDetection;component/Assets/LED-GreenOff.png"));
            _yellowOn = new BitmapImage(new Uri("pack://application:,,,/FallDetection;component/Assets/LED-AmberOn.png"));
            _yellowOff = new BitmapImage(new Uri("pack://application:,,,/FallDetection;component/Assets/LED-AmberOff.png"));

            DataContext = this;
            var blueBrush = new SolidColorBrush(Colors.Blue);
            blueBrush.Freeze();
            var greenBrush = new SolidColorBrush(Colors.Green);
            greenBrush.Freeze();
            var orangeBrush = new SolidColorBrush(Colors.Orange);
            orangeBrush.Freeze();
            var grayBrush = new SolidColorBrush(Colors.DarkGray);
            grayBrush.Freeze();
            _redBrush = new SolidColorBrush(Colors.Red);
            _redBrush.Freeze();
            seriesAd.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = blueBrush,
                    Title = "X",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = greenBrush,
                    Title = "Y",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = orangeBrush,
                    Title = "Z",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = grayBrush,
                    Title = "Total",
                },

            };
            seriesMm.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = blueBrush,
                    Title = "X",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke =greenBrush,
                    Title = "Y",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = orangeBrush,
                    Title = "Z",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = grayBrush,
                    Title = "Total",
                },

            };
            seriesIt.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = blueBrush,
                    Title = "X",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = greenBrush,
                    Title = "Y",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = orangeBrush,
                    Title = "Z",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = grayBrush,
                    Title = "Total",
                },

            };
            seriesEcg.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = blueBrush,
                    Title = "ECG",
                },
                new LineSeries
                {
                    Values =  new ChartValues<ScatterPoint>(),
                    PointGeometrySize = 0,
                    Stroke = orangeBrush,
                    Title = "ECG Filtered",
                }
            };

        }

        public List<float[]> Data;
        public List<float[]> ECGData;
        public Tuple<string, int, float, float, bool, string> Subject;
        private bool _running;
        private bool _paused;
        private readonly BitmapImage _redOn;
        private readonly BitmapImage _redOff;
        private readonly BitmapImage _greenOn;
        private readonly BitmapImage _greenOff;
        private readonly BitmapImage _yellowOn;
        private readonly BitmapImage _yellowOff;
        private SolidColorBrush _redBrush;

        private void BtnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            Data = OpenReadDataFile(tDataFile);
            if (Data != null)
            {
                btnStart.IsEnabled = true;
                var fileName = Path.GetFileNameWithoutExtension(tDataFile.Text);
                var parts = fileName.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                if (!Enum.TryParse(parts[0], out Activities act)) act = Activities.None;
                tAct.Text = $"{parts[0]} : {act.GetDescription()}";
                if (parts.Length < 2 || parts[1].Length < 2 || !Enum.TryParse(parts[1].Substring(0, 2), out SubjectTypes subject)) subject = SubjectTypes.None;
                tSubject.Text = $"{(parts.Length > 1 ? $"{parts[1]} : " : "")}{subject.GetDescription()}";
                if (parts.Length < 3 || !int.TryParse(parts[2].TrimStart('R'), out int trial)) trial = 1;
                tTrial.Text = trial.ToString("000");
                tDuration.Text = (Data.Count * 5 / 1000f).ToString("000.000");
                if (parts.Length > 1)
                {
                    Subject = ReadSubject(parts[1]);
                    if (File.Exists(Subject.Item6)) ECGData = ReadData(Subject.Item6);
                }
            }
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            if (_running) _running = false;
            else StartCalculation();
        }

        private void StartCalculation()
        {
            //  foreach (var tb in gridConfig.Children.OfType<TextBox>()) tb.IsEnabled = false;
            //btnStop.IsEnabled = true;
            // imgAm.Source = greenOn;
            // imgPolice.Source = greenOn;
            // var rand = new Random();
            // var maxAcc = int.Parse(tbMaxAcc.Text);
            // var maxSpeed = int.Parse(tbMaxSpeed.Text);
            // var dirTendency = int.Parse(tbDirTendency.Text) / 100d;
            // var accRate = float.Parse(tbAccRate.Text);
            // var accTendency = int.Parse(tbDirAccTendency.Text) / 100d;
            // var allowedDist = int.Parse(tbMaxDist.Text);
            // var policeDist = allowedDist * (1 - int.Parse(tbPoliceRange.Text) / 100f);
            // var amDist = allowedDist * (1 - int.Parse(tbAmRange.Text) / 100f);
            // var delay = 100;
            // var timeFactor = 1000 / delay;
            // var elsOn = cbEls.IsChecked == true;
            // var elsStatus = elsOn ? greenOn : greenOff;
            Start();
            Task.Run(async () =>
            {
                int delay = 100;
                int bunchFactor = delay / 5;
                int ecgBunchFactor = delay / 2;
                var i = 0;
                var bunchLimit = Data.Count / bunchFactor;
                var wiAd = 0;
                var wiMm = 0;
                var wiIt = 0;
                while (_running && i < bunchLimit)
                {
                    await Task.Delay(delay);
                    if (!_paused)
                    {
                        AddSeriesPointsAverage(i, bunchFactor, seriesAd.Series, 2 * 16f / Math.Pow(2, 13), 0);
                        AddSeriesPointsAverage(i, bunchFactor, seriesIt.Series, 2 * 2000f / Math.Pow(2, 16), 3);
                        AddSeriesPointsAverage(i, bunchFactor, seriesMm.Series, 2 * 8f / Math.Pow(2, 14), 6);
                        CheckForFall(seriesAd.Series[3].Values, imgAd, ref wiAd, i);
                        CheckForFall(seriesMm.Series[3].Values, imgMm, ref wiMm, i);
                        CheckForFall(seriesIt.Series[3].Values, imgIt, ref wiIt, i, 0, 16);
                        Dispatcher.Invoke(() =>
                        {
                            if (imgAd.Source == _redOn || imgMm.Source == _redOn)
                            {
                                if (imgIt.Source == _redOn)
                                {
                                    imgFall.Source = _redOn;
                                    tbPatient.Text =
                                        $"Fall event detected for subject {Subject.Item1} who is a {Subject.Item2} years old {(Subject.Item5 ? "Woman" : "Man")}. {(Subject.Item5 ? "She" : "He")} is {Subject.Item4} KG and {Subject.Item3} CM tall";
                                    tbFall.Text = $"Fall event detected based on the following censors : {(imgAd.Source == _redOn ? "ADXL345 " : "")} {(imgMm.Source == _redOn ? "MMA8451Q " : "")} {(imgIt.Source == _redOn ? "ITG3200 " : "")}";
                                }
                                else
                                    imgFall.Source = _yellowOn;
                            }
                            tTime.Text = ((i + 1) * delay / 1000f).ToString("000.000");
                        });
                        AddECGPoints(i, ecgBunchFactor, seriesEcg.Series);
                        i++;
                    }
                }

                Dispatcher.Invoke(Stop);
            });
        }

        private void Start()
        {
            btnStart.Content = "Stop";
            ClearSeries(seriesAd);
            ClearSeries(seriesMm);
            ClearSeries(seriesIt);
            ClearSeries(seriesEcg);
            _running = true;
            btnPause.IsEnabled = true;
            imgAd.Source = _greenOn;
            imgMm.Source = _greenOn;
            imgIt.Source = _greenOn;
            imgFall.Source = _greenOn;
            tbFall.Text = "";
            tbPatient.Text = "";
            tTime.Text = "000.000";
        }

        private void Stop()
        {
            imgAd.Source = imgAd.Source == _yellowOn ? _yellowOff : imgAd.Source == _redOn ? _redOff : _greenOff;
            imgMm.Source = imgMm.Source == _yellowOn ? _yellowOff : imgMm.Source == _redOn ? _redOff : _greenOff;
            imgIt.Source = imgIt.Source == _yellowOn ? _yellowOff : imgIt.Source == _redOn ? _redOff : _greenOff;
            imgFall.Source = imgFall.Source == _yellowOn ? _yellowOff : imgFall.Source == _redOn ? _redOff : _greenOff;
            btnPause.IsEnabled = false;
            btnStart.Content = "Start";
            _running = false;
            Play();
        }

        private bool CheckForFall(IChartValues values, Image indicator, ref int warningIndex, int i, int mean = 1, double sdFactor = 0.1)
        {
            var count = values.Count;
            if (count >= 30)
            {
                var f4 = values.Cast<ScatterPoint>().Skip(count - 30).Take(20).Select(v => v.Y).StdDev(mean);
                var s1 = values.Cast<ScatterPoint>().Skip(count - 5).Take(5).Select(v => v.Y).StdDev(mean);
                if (s1 / f4 > 2.5)
                {
                    warningIndex = i;
                    Dispatcher.Invoke(() =>
                    {
                        indicator.Source = _yellowOn;
                        ((LineSeries) seriesEcg.Series[0]).Fill = _redBrush;

                    });
                }

                if (warningIndex > 0 && i - warningIndex >= 20)
                {
                    if (i - warningIndex < 55)
                    {
                        var vals = values.Cast<ScatterPoint>().Skip(count - 20).Take(20).Select(v => v.Y).ToArray();
                        var sd = vals.StdDev(mean);
                        if (sd < sdFactor)
                        {
                            warningIndex = i;
                            Dispatcher.Invoke(() =>
                            {
                                if (indicator.Source != _redOn)
                                {
                                    indicator.Source = _redOn;
                                    Pause();
                                }
                            });
                            return true;
                        }
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            indicator.Source = _greenOn;
                            ((LineSeries)seriesEcg.Series[0]).Fill = null;
                        });
                        warningIndex = 0;
                    }
                }
            }

            return false;
        }

        // Return the standard deviation of an array of Doubles.
        //
        // If the second argument is True, evaluate as a sample.
        // If the second argument is False, evaluate as a population.
        private void ClearSeries(CartesianChart chart)
        {
            foreach (var s in chart.Series) s.Values.Clear();
        }

        private double[] AddSeriesPointsAverage(int i, int bunchFactor, SeriesCollection series, double factor, int offset)
        {
            var di = i * bunchFactor;
            double x = 0, y = 0, z = 0, total = 0;
            for (int j = 0; j < bunchFactor && di < Data.Count; j++, di = j + i * bunchFactor)
            {
                x += factor * Data[di][offset + 0];
                y += factor * Data[di][offset + 1];
                z += factor * Data[di][offset + 2];
            }
            x = x / bunchFactor;
            y = y / bunchFactor;
            z = z / bunchFactor;
            total += Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2));
            var time = (5f * di) / 1000;
            Dispatcher.Invoke(() =>
            {
                if (series[0].Values.Count >= 100)
                {
                    series[0].Values.RemoveAt(0);
                    series[1].Values.RemoveAt(0);
                    series[2].Values.RemoveAt(0);
                    series[3].Values.RemoveAt(0);
                }
                series[0].Values.Add(new ScatterPoint(time, x));
                series[1].Values.Add(new ScatterPoint(time, y));
                series[2].Values.Add(new ScatterPoint(time, z));
                series[3].Values.Add(new ScatterPoint(time, total));
            });
            return new[] { x, y, z, total };
        }
        private void AddSeriesPoints(int i, int bunchFactor, SeriesCollection series, double factor, int offset)
        {
            int j = bunchFactor - 1;
            var di = j + i * bunchFactor;
            for (; j < bunchFactor && di < Data.Count; j++, di = j + i * bunchFactor)
            {
                var x = factor * Data[di][offset + 0];
                var y = factor * Data[di][offset + 1];
                var z = factor * Data[di][offset + 2];
                var total = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2));
                var time = (5f * di) / 1000;
                Dispatcher.Invoke(() =>
                {
                    if (series[0].Values.Count >= 100)
                    {
                        series[0].Values.RemoveAt(0);
                        series[1].Values.RemoveAt(0);
                        series[2].Values.RemoveAt(0);
                        series[3].Values.RemoveAt(0);
                    }
                    series[0].Values.Add(new ScatterPoint(time, x));
                    series[1].Values.Add(new ScatterPoint(time, y));
                    series[2].Values.Add(new ScatterPoint(time, z));
                    series[3].Values.Add(new ScatterPoint(time, total));
                });

            }
        }
        private void AddECGPoints(int i, int bunchFactor, SeriesCollection series)
        {
            if (ECGData == null) return;
            var j = bunchFactor - 1;
            var di = j + i * bunchFactor;
            for (; j < bunchFactor && di < ECGData.Count; j++, di = j + i * bunchFactor)
            {
                var x = ECGData[di][1];
                var y = ECGData[di][2];
                var time = (2f * di) / 1000;
                Dispatcher.Invoke(() =>
                {
                    if (series[0].Values.Count >= 100)
                    {
                        series[0].Values.RemoveAt(0);
                        series[1].Values.RemoveAt(0);
                    }
                    series[0].Values.Add(new ScatterPoint(time, x));
                    series[1].Values.Add(new ScatterPoint(time, y));
                });

            }
        }
        private void AddECGPointsAverage(int i, int bunchFactor, SeriesCollection series)
        {
            if (ECGData == null) return;
            double x = 0, y = 0;
            var di = i * bunchFactor;
            for (int j = 0; j < bunchFactor && di < ECGData.Count; j++, di = j + i * bunchFactor)
            {
                x += ECGData[di][1];
                y += ECGData[di][2];
            }
            x = x / bunchFactor;
            y = y / bunchFactor;
            var time = (2f * di) / 1000;
            Dispatcher.Invoke(() =>
            {
                if (series[0].Values.Count >= 100)
                {
                    series[0].Values.RemoveAt(0);
                    series[1].Values.RemoveAt(0);
                }
                series[0].Values.Add(new ScatterPoint(time, x));
                series[1].Values.Add(new ScatterPoint(time, y));
            });
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            if (_paused) Play();
            else Pause();
        }

        private void Play()
        {
            btnPause.Content = "Pause";
            _paused = false;
        }

        private void Pause()
        {
            btnPause.Content = "Play";
            _paused = true;
        }

        private void BtnEcgFile_Copy_Click(object sender, RoutedEventArgs e)
        {
            ECGData = OpenReadDataFile(tEcgFile);
        }

        private List<float[]> OpenReadDataFile(TextBlock tb)
        {
            var fileDialog = new OpenFileDialog { Filter = "Data Text Files|*.txt;*.csv" };
            if (fileDialog.ShowDialog(this) == true)
            {
                Stop();
                var fileName = fileDialog.FileName;
                var data = ReadData(fileName);
                tb.Text = fileName;
                return data;
            }

            return null;
        }

        private static List<float[]> ReadData(string fileName)
        {
            var sr = File.OpenText(fileName);
            var data = new List<float[]>();
            string line;
            while (!sr.EndOfStream && (line = sr.ReadLine()) != null)
            {
                var entries = line.Trim(' ', ';').Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (float.TryParse(entries[entries.Length - 1], out var i))
                {
                    data.Add(entries.Select(float.Parse).ToArray());
                }
            }

            return data;
        }

        private static Tuple<string, int, float, float, bool, string> ReadSubject(string subjectCode)
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Subjects");
            var sr = File.OpenText(Path.Combine(filePath, "Subjects.csv"));
            string line;
            while (!sr.EndOfStream && (line = sr.ReadLine()) != null)
            {
                var entries = line.Trim(' ', ';').Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (entries[0].ToLowerInvariant() == subjectCode.ToLowerInvariant())
                {
                    return new Tuple<string, int, float, float, bool, string>(entries[0], int.Parse(entries[1]),
                        float.Parse(entries[2]), float.Parse(entries[3]), entries[4].ToLower() == "f", Path.Combine(filePath, entries[5]));
                }
            }
            return null;
        }

    }
}
