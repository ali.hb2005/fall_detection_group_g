﻿using System.ComponentModel;

namespace FallDetection
{
    public enum Activities
    {
        [Description("Activity type is unknown")]
        None,
        [Description("Walking slowly")]
        D01,
        [Description("Walking quickly")]
        D02,
        [Description("Jogging slowly")]
        D03,
        [Description("Jogging quickly")]
        D04,
        [Description("Walking upstairs and downstairs slowly")]
        D05,
        [Description("Walking upstairs and downstairs quickly")]
        D06,
        [Description("Slowly sit in a half height chair, wait a moment, and up slowly  ")]
        D07,
        [Description("Quickly sit in a half height chair, wait a moment, and up quickly")]
        D08,
        [Description("Slowly sit in a low height chair, wait a moment, and up slowly")]
        D09,
        [Description("Quickly sit in a low height chair, wait a moment, and up quickly")]
        D10,
        [Description("Sitting a moment, trying to get up, and collapse into a chair")]
        D11,
        [Description("Sitting a moment, lying slowly, wait a moment, and sit again")]
        D12,
        [Description("Sitting a moment, lying quickly, wait a moment, and sit again")]
        D13,
        [Description("Being on one’s back change to lateral position, wait a moment, and change to one’s back")]
        D14,
        [Description("Standing, slowly bending at knees, and getting up")]
        D15,
        [Description("Standing, slowly bending without bending knees, and getting up")]
        D16,
        [Description("Standing, get into a car, remain seated and get out of the car")]
        D17,
        [Description("Stumble while walking")]
        D18,
        [Description("Gently jump without falling (trying to reach a high object)")]
        D19,
        [Description("Fall forward while walking caused by a slip")]
        F01,
        [Description("Fall backward while walking caused by a slip")]
        F02,
        [Description("Lateral fall while walking caused by a slip")]
        F03,
        [Description("Fall forward while walking caused by a trip")]
        F04,
        [Description("Fall forward while jogging caused by a trip")]
        F05,
        [Description("Vertical fall while walking caused by fainting")]
        F06,
        [Description("Fall while walking, with use of hands in a table to dampen fall, caused by fainting")]
        F07,
        [Description("Fall forward when trying to get up")]
        F08,
        [Description("Lateral fall when trying to get up")]
        F09,
        [Description("Fall forward when trying to sit down")]
        F10,
        [Description("Fall backward when trying to sit down")]
        F11,
        [Description("Lateral fall when trying to sit down")]
        F12,
        [Description("Fall forward while sitting, caused by fainting or falling asleep")]
        F13,
        [Description("Fall backward while sitting, caused by fainting or falling asleep")]
        F14,
        [Description("Lateral fall while sitting, caused by fainting or falling asleep")]
        F15
    }
}