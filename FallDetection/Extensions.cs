﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace FallDetection
{
    public static class Extensions
    {
        public static double StdDev(this IEnumerable<double> values, double mean, bool asSample = false)
        {
            // Get the mean.
            var arrValues = values as double[] ?? values.ToArray();
            //double mean = arrValues.Sum() / arrValues.Length;

            // Get the sum of the squares of the differences
            // between the values and the mean.
            double sumOfSquares = arrValues.Select(value => (value - mean) * (value - mean)).Sum();

            return asSample ? Math.Sqrt(sumOfSquares / (arrValues.Length - 1)) : Math.Sqrt(sumOfSquares / arrValues.Length);
        }
        public static string GetDescription(this Enum value)
        {
            // Get the type
            Type type = value.GetType();
            // Get fieldinfo for this type
            FieldInfo fieldInfo = type.GetField(value.ToString());
            var attDesc = fieldInfo.GetCustomAttribute<DescriptionAttribute>();
            return attDesc?.Description;
        }
    }
}
